#!/usr/bin/env node

const findInFiles = require("find-in-files");

const args = process.argv.slice(2);

if (args.length === 0 || args.length > 1) {
  console.error("pass the frappe app directory name without trailing slash");
  process.exit(1);
}

findInFiles
  .find(new RegExp(/@frappe.whitelist.*(.|[\n\r])def.*/), args[0], ".py$")
  .then(found => {
    const guestFunctions = [];
    const userFunctions = [];

    for (const file of Object.keys(found)) {
      const path = args[0].endsWith("/") ? "/" : "";
      const pyModule = file
        .replace(".py", "")
        .replace(path, "")
        .replace(/\//g, ".");
      for (const match of found[file].matches) {
        const pyFunction = match
          .replace(/@frappe.whitelist.*(.|\n)def /, "")
          .replace(":", "");
        const functionPath = (pyModule + "." + pyFunction).replace(
          ".__init__",
          ""
        );
        if (match.includes("allow_guest=True")) {
          guestFunctions.push(functionPath);
        } else {
          userFunctions.push(functionPath);
        }
      }
    }
    console.dir({ guestFunctions, userFunctions }, { maxArrayLength: null });
  });
